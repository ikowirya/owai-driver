package com.owaidriver.eklanku.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.owaidriver.eklanku.BuildConfig;
import com.owaidriver.eklanku.MainActivity;
import com.owaidriver.eklanku.R;
import com.owaidriver.eklanku.database.DBHandler;
import com.owaidriver.eklanku.database.Queries;
import com.owaidriver.eklanku.model.Driver;
import com.owaidriver.eklanku.model.ResponseCheckOtu;
import com.owaidriver.eklanku.network.AppController;
import com.owaidriver.eklanku.network.HTTPHelper;
import com.owaidriver.eklanku.network.Log;
import com.owaidriver.eklanku.network.NetworkActionResult;
import com.owaidriver.eklanku.network.OtuApi;
import com.owaidriver.eklanku.network.OtuService;
import com.owaidriver.eklanku.preference.SettingPreference;
import com.owaidriver.eklanku.preference.UserPreference;
import com.owaidriver.eklanku.service.MyFirebaseInstanceIdService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Locale;

import retrofit2.Callback;

//import net.gumcode.drivermangjek.preference.UserPreference;

public class LoginActivity extends AppCompatActivity {

    Button login;
    EditText email, password;
    LoginActivity activity;
    Driver user;
//    UserPreference userPreference;
    Intent locSev;
    String token = "";
    FirebaseInstanceId fireId;
    int maxRetry = 4;
    OtuService otuService;
    String securityCode;
    String mbr_token,idUser,userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        otuService = OtuApi.getClient().create(OtuService.class);
        activity = LoginActivity.this;
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);

        locSev = new Intent(this, MyFirebaseInstanceIdService.class);
        startService(locSev);

//        userPreference = new UserPreference(activity);
        login = findViewById(R.id.loginButton);
        new SettingPreference(this).insertSetting(new String[]{"OFF", "0", "OFF", String.valueOf(BuildConfig.VERSION_CODE)});



//        Log.d("NewToken", token);
//        Log.d("NewToken", userPreference.getDriver().gcm_id);

        ImageView clear = findViewById(R.id.clearEmail);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email.setText("");
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fireId = FirebaseInstanceId.getInstance();
                if(fireId.getToken() != null)
                    token = fireId.getToken();

                if(token.equals("")){
                    Toast.makeText(activity, "Waiting for Connection..", Toast.LENGTH_SHORT).show();
//                    showWarning();
                }else{
                    if(email.getText().toString().equals("") || password.getText().toString().equals("")){
                        Toast.makeText(activity, "Please complete the input form", Toast.LENGTH_SHORT).show();
                    }else{
//                        if(password.getText().toString().length() < 7){
//                            Toast.makeText(activity, "Minimum password 6 karakter", Toast.LENGTH_SHORT).show();
//                        }
                        Calendar calendar = Calendar.getInstance(Locale.getDefault());
                        int hour = calendar.get(Calendar.HOUR_OF_DAY);
                        int minute = calendar.get(Calendar.MINUTE);
                        int second = calendar.get(Calendar.SECOND);
                        int date = calendar.get(Calendar.DAY_OF_MONTH);
                        int month = calendar.get(Calendar.MONTH);
                        int year = calendar.get(Calendar.YEAR);
                        String tgl = String.valueOf(year)+"-"+String.valueOf(month)+"-"+String.valueOf(date);
                        String time = String.valueOf(hour)+":"+String.valueOf(minute)+":"+String.valueOf(second);
                        String tokenOTU = tgl+"T"+time;
                        String ecrpt = md5(password.getText()+"x@2564D");
                        String ecrpt2 = md5(ecrpt);
                        securityCode = md5(token+ecrpt2);
                        loginOtu(email.getText().toString(),tokenOTU,securityCode,password.getText().toString(),"OWAIDRIVER");
//                        signin(token);
                    }
                }

            }
        });
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    private void loginOtu(String valueUserID,final String tokenOTU,String securityCode,String valuePassword,String valueAplUse) {
        retrofit2.Call<ResponseCheckOtu> users = otuService.loginOtu(valueUserID,tokenOTU,securityCode,valuePassword,valueAplUse);
        users.enqueue(new Callback<ResponseCheckOtu>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseCheckOtu> call, retrofit2.Response<ResponseCheckOtu> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals("SUCCESS"))
                    {
                         mbr_token =  response.body().getMbr_token();
                         userID = response.body().getUserID();

                        SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref",0);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("mbr_token",mbr_token);
                        editor.putString("userID",userID);
                        editor.putString("token",tokenOTU);
                        editor.commit();
                        signin(token);
//                        sendToken(mbr_token,userID);
//                        Toast.makeText(LoginActivity.this, "Login Successful", Toast.LENGTH_SHORT).show();
//                        finish();
                    }

                    else {
                        Snackbar.make(findViewById(R.id.activity_splash), ""+ response.body().getRespMessage() , Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseCheckOtu> call, Throwable t) {
                android.util.Log.e("TAG", "onFailure: " + t.toString());
                t.printStackTrace();
                Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }



    private void signin(final String token){
        final JSONObject logData = new JSONObject();
        try {
//            logData.put("email", email.getText().toString());
            logData.put("no_telepon", email.getText().toString());
            logData.put("password", password.getText().toString());
            logData.put("reg_id", token);
        } catch (JSONException e) {
            e.printStackTrace();
        }

//        Log.d("login_data", logData.toString());
        final ProgressDialog pd = showLoading();
        HTTPHelper.getInstance(activity).login(logData, new NetworkActionResult() {
            @Override
            public void onSuccess(JSONObject obj) {
                Log.d("login_data", obj.toString());
                try {
                    if(obj.getString("message").equals("found")){
                        pd.dismiss();
                        user = HTTPHelper.parseUserJSONData(activity, obj.toString());
                        user.gcm_id = token;
                        user.password = password.getText().toString();
                        Queries que = new Queries(new DBHandler(activity));
                        que.insertDriver(user);
                        que.closeDatabase();
                        sendToken(mbr_token,user.id);
                        new UserPreference(activity).insertDriver(user);
                        if(loadImageFromServer(user.image)){
                            FirebaseMessaging.getInstance().subscribeToTopic("info");

                        }
                    }else if(obj.getString("message").equals("banned")){
                        pd.dismiss();
                        showWarning("Your account has been banned, please confirm to the Owai ffice");
                    }else{
                        pd.dismiss();
                        showWarning("Incorrect mobile number or password");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                maxRetry = 4;
            }

            @Override
            public void onFailure(String message) {

            }

            @Override
            public void onError(String message) {
//                Log.d("Pesan_error", message);
                if(maxRetry == 0){
                    pd.dismiss();
                    showWarning();
                    maxRetry = 4;
                }else{
                    signin(token);
                    maxRetry--;
                    Log.d("Try_ke_login", String.valueOf(maxRetry));
                    pd.dismiss();
                }

            }
        });
    }

    private void sendToken(final String mbr_token, final String idUser){
        final JSONObject logData = new JSONObject();
        try {
//            logData.put("email", email.getText().toString());
            logData.put("access_token", mbr_token);
            logData.put("id_driver", idUser);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HTTPHelper.getInstance(activity).updateAccessTokenOtu(logData, new NetworkActionResult() {
            @Override
            public void onSuccess(JSONObject obj) {
                Log.d("hayo", obj.toString());
                try {
                    if(obj.getString("message").equals("success")) {
                        Intent change = new Intent(activity, MainActivity.class);
                        startActivity(change);
                        finish();
                    }else{
                        Toast.makeText(activity, "Failed!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                maxRetry = 4;
            }

            @Override
            public void onFailure(String message) {

            }

            @Override
            public void onError(String message) {
//                Log.d("Pesan_error", message);
                if(maxRetry == 0){
                    showWarning();
                    maxRetry = 4;
                }else{
                    sendToken(mbr_token,idUser);
                    maxRetry--;
                    Log.d("Try_ke_login", String.valueOf(maxRetry));
                }

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(locSev);
    }

    private ProgressDialog showLoading(){
        ProgressDialog ad = ProgressDialog.show(activity, "", "Loading...", true);
        return ad;
    }


    private MaterialDialog showWarning() {
        final MaterialDialog md = new MaterialDialog.Builder(activity)
                .title("Connection is problem")
                .content("Try Again!")
                .icon(new IconicsDrawable(activity)
                        .icon(FontAwesome.Icon.faw_exclamation_triangle)
                        .color(Color.YELLOW)
                        .sizeDp(24))
                .positiveText("Close")
                .positiveColor(Color.DKGRAY)
                .show();

        View positive = md.getActionButton(DialogAction.POSITIVE);

        positive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                md.dismiss();
            }
        });
        return md;
    }

    private MaterialDialog showWarning(String content) {
        final MaterialDialog md = new MaterialDialog.Builder(activity)
                .title("Login Failed")
                .content(content)
                .icon(new IconicsDrawable(activity)
                        .icon(FontAwesome.Icon.faw_exclamation_triangle)
                        .color(Color.RED)
                        .sizeDp(20))
                .positiveText("Close")
                .positiveColor(Color.DKGRAY)
                .show();

        View positive = md.getActionButton(DialogAction.POSITIVE);

        positive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                md.dismiss();
            }
        });
        return md;
    }

    private boolean loadImageFromServer(String image) {
        final ProgressDialog pd = showLoading();
        ImageLoader imageLoader = AppController.getInstance(this).getImageLoader();
        imageLoader.get(image, new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
//                Log.d("Image Load Error: ", error.getMessage());
                pd.dismiss();
            }
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    Bitmap circleBitmap = response.getBitmap();
                    saveToInternalStorage(circleBitmap);
                }
                pd.dismiss();
            }
        });
        return true;
    }

    private String saveToInternalStorage(Bitmap bitmapImage){
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        File directory = cw.getDir("fotoDriver", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory, "profile.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 50, fos);
            fos.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return directory.getAbsolutePath();
    }
}
