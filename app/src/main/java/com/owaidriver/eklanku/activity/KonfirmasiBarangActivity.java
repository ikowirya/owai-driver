package com.owaidriver.eklanku.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.owaidriver.eklanku.R;
import com.owaidriver.eklanku.database.DBHandler;
import com.owaidriver.eklanku.database.Queries;
import com.owaidriver.eklanku.model.Content;
import com.owaidriver.eklanku.model.Driver;
import com.owaidriver.eklanku.model.PaymentResponseOtu;
import com.owaidriver.eklanku.model.Transaksi;
import com.owaidriver.eklanku.network.AsyncTaskHelper;
import com.owaidriver.eklanku.network.HTTPHelper;
import com.owaidriver.eklanku.network.Log;
import com.owaidriver.eklanku.network.NetworkActionResult;
import com.owaidriver.eklanku.network.OtuApi;
import com.owaidriver.eklanku.network.OtuService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Callback;

import static android.os.Build.VERSION_CODES.N;

//import net.gumcode.drivermangjek.preference.UserPreference;

public class KonfirmasiBarangActivity extends AppCompatActivity {

    private static final String TAG = "TAG: finishFood";
    KonfirmasiBarangActivity activity;
    private static final int TAKE_PICTURE = 1;
    private Uri imageUri;
    ImageView uploadFoto;
    String fotoInvoice = "";
    private File file;
    private byte[] bytes;
    Transaksi myTrans;
    int status;
    TextView butSubmit;
    Queries que;
    Toolbar toolbar;
    Driver driver;
    int maxRetry = 4;
    OtuService otuService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_konfirmasi_barang);
        otuService = OtuApi.getClient().create(OtuService.class);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.md_nav_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Upload Receipt Shopping");

        activity = KonfirmasiBarangActivity.this;
        myTrans = (Transaksi) getIntent().getSerializableExtra("transaksi");

        que = new Queries(new DBHandler(activity));
        driver = que.getDriver();
        initView();
    }

    private void initView(){
        butSubmit = findViewById(R.id.butSubmitU);
        uploadFoto = findViewById(R.id.uploadFoto);
        final EditText totalBiaya = findViewById(R.id.totalBiaya);
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            butSubmit.setEnabled(false);
            ActivityCompat.requestPermissions(activity, new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
        }

        uploadFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture();
            }
        });

        butSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isOk1, isOk2 = false;
                if(totalBiaya.getText().toString().equals("")){
                    Toast.makeText(activity, "Please enter the total cost.", Toast.LENGTH_SHORT).show();
                    isOk1 = false;
                }else{
                    isOk1 = true;
                }
                if(fotoInvoice.equals("")){
                    Toast.makeText(activity, "Please enter a photo of the shopping receipt.", Toast.LENGTH_SHORT).show();
                    isOk2 = false;
                }else{
                    isOk2 = true;
                }

                if(isOk1 && isOk2){
                    JSONObject jFins = new JSONObject();
                    try {
                        jFins.put("id", driver.id);
                        jFins.put("id_transaksi", myTrans.id_transaksi);
                        jFins.put("foto_struk", fotoInvoice);
                        jFins.put("harga_akhir", totalBiaya.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if(String.valueOf(myTrans.order_fitur).equals("4")){
                        finishTransaksiMmart(jFins);
                    }else{
                        finishTransaksiMfood(jFins);
                    }
                }
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        switch (requestCode) {
//            case TAKE_PICTURE:
//                if (resultCode == Activity.RESULT_OK) {
//                    Uri selectedImage = imageUri;
//                    getContentResolver().notifyChange(selectedImage, null);
//                    ContentResolver cr = getContentResolver();
//                    Bitmap bitmap;
//                    try {
//                        bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, selectedImage);
//                        fotoInvoice = compressJSON(bitmap);
//
//                        uploadFoto.setImageBitmap(bitmap);
////                        Toast.makeText(activity, selectedImage.toString(), Toast.LENGTH_LONG).show();
//                    } catch (Exception e) {
//                        Toast.makeText(activity, "Failed to load", Toast.LENGTH_SHORT).show();
////                        Log.e("Camera", e.toString());
//                    }
//                }
//                break;
//            default:
//                break;
//        }
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
//                File newf = new File(file.getPath());
//                Uri uri = Uri.fromFile(newf);

                Bitmap bm = BitmapFactory.decodeFile(file.getPath());
                uploadFoto.setImageBitmap(bm);
                bytes = convertBitmapToByteArray(bm);
                fotoInvoice = "data:image/jpeg;base64,"+ Base64.encodeToString(bytes, Base64.DEFAULT);

            } else {
                file.delete();
            }
        }
    }

    private byte[] convertBitmapToByteArray(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        return baos.toByteArray();
    }

    private void takePicture() {
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File newdir = new File(dir.getPath() + "/public/");
        newdir.mkdir();

        file = new File(newdir + "/" + System.currentTimeMillis() + ".jpg");

        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Uri uri;
        Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (Build.VERSION.SDK_INT > N) {
            uri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".fileProvider", file);
            intentCamera.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else {
            uri = Uri.fromFile(file);
        }
        intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intentCamera,1);
    }

    public void takePhoto() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "Invoice_" + timeStamp +".jpg";

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File photo = new File(Environment.getExternalStorageDirectory(),  imageFileName);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(photo));
            imageUri = Uri.fromFile(photo);
        } else {
            File file = new File(photo.getPath());
            Uri photoUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + ".fileProvider", file);
            imageUri = photoUri;
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        }
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(intent, TAKE_PICTURE);
    }


    public String compressJSON(Bitmap bmp){
        byte[] imageBytes0;
        ByteArrayOutputStream baos0 = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG ,50, baos0);
        imageBytes0 = baos0.toByteArray();

        //image.setImageBitmap(bmp);

        String encodedImage= Base64.encodeToString(imageBytes0, Base64.DEFAULT);
        return encodedImage;
    }

    private void finishTransaksiMmart(final JSONObject jFins){
        final ProgressDialog pd= showLoading();
        HTTPHelper.getInstance(activity).driverFinishMamrt(jFins, new NetworkActionResult() {
            @Override
            public void onSuccess(JSONObject obj) {
                pd.dismiss();
                try {
                    if(obj.getString("message").equals("finish")){
                        JSONArray jArray = obj.getJSONArray("data");
                        JSONObject oneObject = jArray.getJSONObject(0);
                        String id_transaksi = oneObject.getString("id_transaksi");
                        String biaya_akhir = oneObject.getString("biaya_akhir");
                        final String pakai_mpay = oneObject.getString("pakai_mpay");
                        JSONObject jsonObject = obj.getJSONObject("otu");
                        String ekl_driver = jsonObject.getString("ekl_driver");
                        String ekl_customer = jsonObject.getString("ekl_customer");
                        String access_token = jsonObject.getString("access_token");
                        String potongan_customer = jsonObject.getString("potongan_customer");
                        String method;

                        if (pakai_mpay.equals("1")) {
                            method = "SALDO";
                            String kredit_driver = jsonObject.getString("kredit_driver");
                            paymentOtu("OWAIDRIVER",access_token,ekl_driver,ekl_customer,method,biaya_akhir,kredit_driver,id_transaksi);
                        }else {
                            method = "TUNAI";
                            String debit_driver = jsonObject.getString("debit_driver");
                            paymentOtu("OWAIDRIVER",access_token,ekl_driver,ekl_customer,method,biaya_akhir,debit_driver,id_transaksi);
                        }

                        announceToUser(myTrans.id_transaksi, 4, myTrans.order_fitur);
                    }else{
                        Toast.makeText(activity, "Connection Problem..", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                maxRetry = 4;
            }

            @Override
            public void onFailure(String message) {

            }

            @Override
            public void onError(String message) {
                if(maxRetry == 0){
                    pd.dismiss();
                    Toast.makeText(activity, "Connection Problem...", Toast.LENGTH_SHORT).show();
                    maxRetry = 4;
                }else{
                    finishTransaksiMmart(jFins);
                    maxRetry--;
                    Log.d("Try_ke_konfirmasi", String.valueOf(maxRetry));
                    pd.dismiss();
                }
            }
        });
    }

    private void finishTransaksiMfood(final JSONObject jFins){
        final ProgressDialog pd= showLoading();
        HTTPHelper.getInstance(activity).driverFinishMfood(jFins, new NetworkActionResult() {
            @Override
            public void onSuccess(JSONObject obj) {
                pd.dismiss();
                try {
                    if(obj.getString("message").equals("finish")){
                        JSONArray jArray = obj.getJSONArray("data");
                        JSONObject oneObject = jArray.getJSONObject(0);
                        String id_transaksi = oneObject.getString("id_transaksi");
                        String biaya_akhir = oneObject.getString("biaya_akhir");
                        final String pakai_mpay = oneObject.getString("pakai_mpay");
                        JSONObject jsonObject = obj.getJSONObject("otu");
                        String ekl_driver = jsonObject.getString("ekl_driver");
                        String ekl_customer = jsonObject.getString("ekl_customer");
                        String access_token = jsonObject.getString("access_token");
                        String potongan_customer = jsonObject.getString("potongan_customer");
                        String method;
                        if (pakai_mpay.equals("1")) {
                            method = "SALDO";
                            String kredit_driver = jsonObject.getString("kredit_driver");
                            paymentOtu("OWAIDRIVER",access_token,ekl_driver,ekl_customer,method,biaya_akhir,kredit_driver,id_transaksi);
                        }else {
                            method = "TUNAI";
                            String debit_driver = jsonObject.getString("debit_driver");
                            paymentOtu("OWAIDRIVER",access_token,ekl_driver,ekl_customer,method,biaya_akhir,debit_driver,id_transaksi);
                        }

                        announceToUser(myTrans.id_transaksi, 4, myTrans.order_fitur);
                    }else{
                        Toast.makeText(activity, "There is an error", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                maxRetry = 4;
            }

            @Override
            public void onFailure(String message) {

            }

            @Override
            public void onError(String message) {
                if(maxRetry == 0){
                    pd.dismiss();
                    Toast.makeText(activity, "Connection Problem", Toast.LENGTH_SHORT).show();
                    maxRetry = 4;
                }else{
                    finishTransaksiMfood(jFins);
                    maxRetry--;
                    Log.d("Try_ke_konfirmasi", String.valueOf(maxRetry));
                    pd.dismiss();
                }
            }
        });
    }

    private void paymentOtu(String owaicustomer, String access_token, String ekl_driver,
                            String ekl_customer, String method, String amount, String biaya_akhir, String id_transaksi){
        retrofit2.Call<PaymentResponseOtu> users = otuService.paymentOrder(owaicustomer,access_token,
                ekl_driver,ekl_customer,method,amount,biaya_akhir,id_transaksi);
        users.enqueue(new Callback<PaymentResponseOtu>() {
            @Override
            public void onResponse(retrofit2.Call<PaymentResponseOtu> call, retrofit2.Response<PaymentResponseOtu> response) {
                if (response.isSuccessful()) {

                    android.util.Log.d(TAG, "onResponse: "+response.body().getStatus());
                    if (response.body().getStatus().equals("SUCCESS")){

                    }

                    else {

                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<PaymentResponseOtu> call, Throwable t) {
                android.util.Log.e("TAG", "onFailure: " + t.toString());
                t.printStackTrace();
                Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private ProgressDialog showLoading(){
        ProgressDialog ad = ProgressDialog.show(activity, "", "Loading...", true);
        return ad;
    }

    private void announceToUser(String id_trans, int acc, String orderFitur){
        Content content = new Content();
        content.addRegId(myTrans.reg_id_pelanggan);
        content.createDataOrderFins(driver.id, id_trans, String.valueOf(acc), myTrans.order_fitur, myTrans.id_pelanggan, driver.image);
        sendResponseToPelanggan(content);
    }

    private void sendResponseToPelanggan(final Content content){

        AsyncTaskHelper asyncTask = new AsyncTaskHelper(activity, true);
        asyncTask.setAsyncTaskListener(new AsyncTaskHelper.OnAsyncTaskListener() {
            @Override
            public void onAsyncTaskDoInBackground(AsyncTaskHelper asyncTask) {
                status = HTTPHelper.sendToGCMServer(content);
            }

            @Override
            public void onAsyncTaskProgressUpdate(AsyncTaskHelper asyncTask) {
            }

            @Override
            public void onAsyncTaskPostExecute(AsyncTaskHelper asyncTask) {
                if (status == 1) {
                }else{
                    Toast.makeText(activity, "Message sending failed to customer", Toast.LENGTH_SHORT).show();
                }
                que.truncate(DBHandler.TABLE_CHAT);
                que.truncate(DBHandler.TABLE_IN_PROGRESS_TRANSAKSI);
                if(myTrans.order_fitur.equals("3")){
                    que.truncate(DBHandler.TABLE_MAKANAN_BELANJA);
                }else{
                    que.truncate(DBHandler.TABLE_BARANG_BELANJA);
                }
                que.updateStatus(1);

                Intent toRate = new Intent(activity, RatingUserActivity.class);
                toRate.putExtra("id_transaksi", myTrans.id_transaksi);
                toRate.putExtra("id_pelanggan", myTrans.id_pelanggan);
                toRate.putExtra("nama_pelanggan", myTrans.nama_pelanggan);
                toRate.putExtra("order_fitur", myTrans.order_fitur);
                toRate.putExtra("id_driver", driver.id);
                startActivity(toRate);
                finish();
        }
            @Override
            public void onAsyncTaskPreExecute(AsyncTaskHelper asyncTask) {

            }
        });
        asyncTask.execute();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                butSubmit.setEnabled(true);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        que.closeDatabase();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
