package com.owaidriver.eklanku.activity;

import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.owaidriver.eklanku.R;
import com.owaidriver.eklanku.model.ResponseCheckOtu;
import com.owaidriver.eklanku.network.OtuApi;
import com.owaidriver.eklanku.network.OtuService;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Callback;

public class LoginOtuActivity extends AppCompatActivity {

    @BindView(R.id.txtUserID)
    EditText txtUserID;
    @BindView(R.id.txtPassword)
    EditText txtPassword;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.btnRegisterOtu)
    LinearLayout btnRegisterOtu;
    String valueUserID,valuePassword;
    static String valueAplUse;
    String token,securityCode;
    OtuService otuService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_otu);
        ButterKnife.bind(this);
        otuService = OtuApi.getClient().create(OtuService.class);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valueUserID = txtUserID.getText().toString();
                valuePassword = txtPassword.getText().toString();
                if (!valuePassword.isEmpty()||!valueUserID.isEmpty())
                {
                    Calendar calendar = Calendar.getInstance(Locale.getDefault());
                    int hour = calendar.get(Calendar.HOUR_OF_DAY);
                    int minute = calendar.get(Calendar.MINUTE);
                    int second = calendar.get(Calendar.SECOND);
                    int date = calendar.get(Calendar.DAY_OF_MONTH);
                    int month = calendar.get(Calendar.MONTH);
                    int year = calendar.get(Calendar.YEAR);
                    String tgl = String.valueOf(year)+"-"+String.valueOf(month)+"-"+String.valueOf(date);
                    String time = String.valueOf(hour)+":"+String.valueOf(minute)+":"+String.valueOf(second);
                    token = tgl+"T"+time;
                    String ecrpt = md5(valuePassword+"x@2564D");
                    String ecrpt2 = md5(ecrpt);
                    securityCode = md5(token+ecrpt2);
                    loginOtu(valueUserID,token,securityCode,valuePassword,"OWAIDRIVER");
                }else {
                    Snackbar.make(findViewById(R.id.main), "Please, Completed Form Data!", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
        btnRegisterOtu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    private void loginOtu(String valueUserID,final String token,String securityCode,String valuePassword,String valueAplUse) {
        retrofit2.Call<ResponseCheckOtu> users = otuService.loginOtu(valueUserID,token,securityCode,valuePassword,valueAplUse);
        users.enqueue(new Callback<ResponseCheckOtu>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseCheckOtu> call, retrofit2.Response<ResponseCheckOtu> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals("SUCCESS"))
                    {
                        String mbr_token =  response.body().getMbr_token();
                        String userID = response.body().getUserID();

                        SharedPreferences preferences = getApplicationContext().getSharedPreferences("MyPref",0);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("mbr_token",mbr_token);
                        editor.putString("userID",userID);
                        editor.putString("token",token);
                        editor.commit();
                        Toast.makeText(LoginOtuActivity.this, "Login Successful", Toast.LENGTH_SHORT).show();
                        finish();
                    }

                    else {
                        Snackbar.make(findViewById(R.id.main), ""+ response.body().getRespMessage() , Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseCheckOtu> call, Throwable t) {
                Log.e("TAG", "onFailure: " + t.toString());
                t.printStackTrace();
                Toast.makeText(getApplicationContext(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
