package com.owaidriver.eklanku.network;

import com.owaidriver.eklanku.model.BankOtu;
import com.owaidriver.eklanku.model.PaymentResponseOtu;
import com.owaidriver.eklanku.model.ResponseCheckOtu;
import com.owaidriver.eklanku.model.ResponseRegisterOtu;
import com.owaidriver.eklanku.model.ResponseTopUp;
import com.owaidriver.eklanku.model.SaldoBonusOtu;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by Iko Wirya on 4/12/2019.
 */
public interface OtuService {
    @Headers("x-api-key: 222")
    @FormUrlEncoded
    @POST("Member/go_register")
    Call<ResponseRegisterOtu> registerOtu(@Field("aplUse") String aplUse,
                                          @Field("email") String email,
                                          @Field("nama") String nama,
                                          @Field("password") String password,
                                          @Field("pin") String pin,
                                          @Field("uplineID") String uplineID,
                                          @Field("userID") String userID);

    @Headers("x-api-key: 222")
    @FormUrlEncoded
    @POST("Member/login")
    Call<ResponseCheckOtu> loginOtu(@Field("userID") String userID,
                                       @Field("token") String token,
                                       @Field("securityCode") String securityCode,
                                       @Field("passwd") String passwd,
                                       @Field("aplUse") String aplUse);

    @Headers("x-api-key: 222")
    @FormUrlEncoded
    @POST("Member/get_saldo_bonus")
    Call<SaldoBonusOtu> saldoBonus(@Field("userID") String userID,
                                   @Field("accessToken") String accessToken,
                                   @Field("aplUse") String aplUse);

    @Headers("x-api-key: 222")
    @FormUrlEncoded
    @POST("Deposit/bank")
    Call<BankOtu> getBank(@Field("userID") String userID,
                          @Field("accessToken") String accessToken,
                          @Field("aplUse") String aplUse);

    @Headers("x-api-key: 222")
    @FormUrlEncoded
    @POST("Deposit/request")
    Call<ResponseTopUp> requestBank(@Field("userID") String userID,
                                    @Field("accessToken") String accessToken,
                                    @Field("aplUse") String aplUse,
                                    @Field("bank") String bank,
                                    @Field("nominal") String nominal);

    @Headers("x-api-key: 222")
    @FormUrlEncoded
    @POST("Member/kirim_key")
    Call<PaymentResponseOtu> checkKeyOtu(@Field("userID") String userID,
                                         @Field("aplUse") String aplUse,
                                         @Field("accessToken") String accessToken);


    @Headers("x-api-key: 222")
    @FormUrlEncoded
    @POST("Finance/request_withdraw_saldo_driver")
    Call<PaymentResponseOtu> withdrawOtu(@Field("aplUse") String aplUse,
                                    @Field("accessToken") String accessToken,
                                    @Field("userID") String userID,
                                    @Field("jumlah") String jumlah,
                                    @Field("pin") String pin,
                                    @Field("key") String key);


    @Headers("x-api-key: 222")
    @FormUrlEncoded
    @POST("Transport/payment_order")
    Call<PaymentResponseOtu> paymentOrder(@Field("aplUse") String aplUse,
                                          @Field("accessToken") String accessToken,
                                          @Field("driver") String ekl_driver,
                                          @Field("customer") String ekl_customer,
                                          @Field("method") String method,
                                          @Field("amount") String amount,
                                          @Field("driver_fee") String driver_fee,
                                          @Field("invoice") String invoice
                                    );

    @Headers("x-api-key: 222")
    @FormUrlEncoded
    @POST("Member/logout")
    Call<ResponseRegisterOtu> logout(@Field("userID") String userID,
                                @Field("accessToken") String accessToken,
                                @Field("token") String token);

}
