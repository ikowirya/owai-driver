package com.owaidriver.eklanku.network;

import org.json.JSONObject;


public interface NetworkActionResult {
    void onSuccess(JSONObject obj);
    void onFailure(String message);
    void onError(String message);

}