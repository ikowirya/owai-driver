package com.owaidriver.eklanku.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.owaidriver.eklanku.R;
import com.owaidriver.eklanku.model.BankListOtu;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Iko Wirya on 4/16/2019.
 */
public class BankAdapter extends RecyclerView.Adapter<BankAdapter.MyViewHolder> {

    List<BankListOtu> bankListOtuList;

    public BankAdapter(List<BankListOtu> bankListOtuList) {
        this.bankListOtuList = bankListOtuList;
    }

    @NonNull
    @Override
    public BankAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_bank, parent, false);
        BankAdapter.MyViewHolder viewHolder = new BankAdapter.MyViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull BankAdapter.MyViewHolder holder, int i) {
        final Context context = holder.itemView.getContext();
        if (!bankListOtuList.get(i).getIsactive().equals("Block")){
            holder.txtBank.setVisibility(View.VISIBLE);
            holder.txtRek.setVisibility(View.VISIBLE);
            holder.txtAn.setVisibility(View.VISIBLE);
            holder.imgBank.setVisibility(View.VISIBLE);
            holder.line3.setVisibility(View.VISIBLE);
            holder.txtBank.setText(bankListOtuList.get(i).getBank());
            holder.txtAn.setText(bankListOtuList.get(i).getAnbank());
            holder.txtRek.setText(bankListOtuList.get(i).getNorec());
            Glide.with(context).load(bankListOtuList.get(i).getLogo())
                    .thumbnail(0.5f)
                    .into(holder.imgBank);

        }
        else {
            holder.line3.setVisibility(View.GONE);
            holder.txtBank.setVisibility(View.GONE);
            holder.txtRek.setVisibility(View.GONE);
            holder.txtAn.setVisibility(View.GONE);
            holder.imgBank.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return bankListOtuList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtBank)
        TextView txtBank;
        @BindView(R.id.txtAn)
        TextView txtAn;
        @BindView(R.id.txtRek)
        TextView txtRek;
        @BindView(R.id.imgBank)
        ImageView imgBank;
        @BindView(R.id.line3)
        FrameLayout line3;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
