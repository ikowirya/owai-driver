package com.owaidriver.eklanku.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Iko Wirya on 4/12/2019.
 */
public class CheckOtuUser {
    @SerializedName("userID")
    private String userID;
    @SerializedName("aplUse")
    private String aplUse;

    public CheckOtuUser(String userID, String aplUse) {
        this.userID = userID;
        this.aplUse = aplUse;
    }

    public String getUserID() {

        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getAplUse() {
        return aplUse;
    }

    public void setAplUse(String aplUse) {
        this.aplUse = aplUse;
    }
}
