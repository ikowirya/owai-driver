package com.owaidriver.eklanku.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Iko Wirya on 4/15/2019.
 */
public class SaldoBonusOtu {
    @SerializedName("errNumber")
    private String errNumber;
    @SerializedName("userID")
    private String userID;
    @SerializedName("Balance")
    private List<BalanceOtu> Balance;
    @SerializedName("respTime")
    private String respTime;
    @SerializedName("status")
    private String status;
    @SerializedName("respMessage")
    private String respMessage;

    public SaldoBonusOtu(String errNumber, String userID, List<BalanceOtu> balance, String respTime, String status, String respMessage) {
        this.errNumber = errNumber;
        this.userID = userID;
        Balance = balance;
        this.respTime = respTime;
        this.status = status;
        this.respMessage = respMessage;
    }

    public String getErrNumber() {

        return errNumber;
    }

    public void setErrNumber(String errNumber) {
        this.errNumber = errNumber;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public List<BalanceOtu> getBalance() {
        return Balance;
    }

    public void setBalance(List<BalanceOtu> balance) {
        Balance = balance;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }
}
