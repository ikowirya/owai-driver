package com.owaidriver.eklanku.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Iko Wirya on 4/15/2019.
 */
public class BalanceOtu {
    @SerializedName("id_member")
    private String id_member;
    @SerializedName("sisa_uang")
    private Double sisa_uang;
    @SerializedName("carier_member")
    private String carier_member;
    @SerializedName("bonus_member")
    private Double bonus_member;
    @SerializedName("saldo_driver")
    private Double saldo_driver;
    @SerializedName("saldo_merchant")
    private Double saldo_merchant;

    public BalanceOtu(String id_member, Double sisa_uang, String carier_member, Double bonus_member, Double saldo_driver, Double saldo_merchant) {
        this.id_member = id_member;
        this.sisa_uang = sisa_uang;
        this.carier_member = carier_member;
        this.bonus_member = bonus_member;
        this.saldo_driver = saldo_driver;
        this.saldo_merchant = saldo_merchant;
    }

    public String getId_member() {
        return id_member;
    }

    public void setId_member(String id_member) {
        this.id_member = id_member;
    }

    public Double getSisa_uang() {
        return sisa_uang;
    }

    public void setSisa_uang(Double sisa_uang) {
        this.sisa_uang = sisa_uang;
    }

    public String getCarier_member() {
        return carier_member;
    }

    public void setCarier_member(String carier_member) {
        this.carier_member = carier_member;
    }

    public Double getBonus_member() {
        return bonus_member;
    }

    public void setBonus_member(Double bonus_member) {
        this.bonus_member = bonus_member;
    }

    public Double getSaldo_driver() {
        return saldo_driver;
    }

    public void setSaldo_driver(Double saldo_driver) {
        this.saldo_driver = saldo_driver;
    }

    public Double getSaldo_merchant() {
        return saldo_merchant;
    }

    public void setSaldo_merchant(Double saldo_merchant) {
        this.saldo_merchant = saldo_merchant;
    }
}
