package com.owaidriver.eklanku.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Iko Wirya on 4/16/2019.
 */
public class RequestTopup {
    @SerializedName("userID")
    private String userID;
    @SerializedName("accessToken")
    private String accessToken;
    @SerializedName("aplUse")
    private String aplUse;
    @SerializedName("bank")
    private String bank;
    @SerializedName("nominal")
    private String nominal;

    public RequestTopup(String userID, String accessToken, String aplUse, String bank, String nominal) {
        this.userID = userID;
        this.accessToken = accessToken;
        this.aplUse = aplUse;
        this.bank = bank;
        this.nominal = nominal;
    }

    public String getUserID() {

        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAplUse() {
        return aplUse;
    }

    public void setAplUse(String aplUse) {
        this.aplUse = aplUse;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getNominal() {
        return nominal;
    }

    public void setNominal(String nominal) {
        this.nominal = nominal;
    }
}
