package com.owaidriver.eklanku.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Iko Wirya on 4/18/2019.
 */
public class ResponseTopUp {
    @SerializedName("errNumber")
    private String errNumber;
    @SerializedName("userID")
    private String userID;
    @SerializedName("bank")
    private String bank;
    @SerializedName("nominal")
    private String nominal;
    @SerializedName("respTime")
    private String respTime;
    @SerializedName("status")
    private String status;
    @SerializedName("respMessage")
    private String respMessage;

    public ResponseTopUp(String errNumber, String userID, String bank, String nominal, String respTime, String status, String respMessage) {
        this.errNumber = errNumber;
        this.userID = userID;
        this.bank = bank;
        this.nominal = nominal;
        this.respTime = respTime;
        this.status = status;
        this.respMessage = respMessage;
    }

    public String getErrNumber() {

        return errNumber;
    }

    public void setErrNumber(String errNumber) {
        this.errNumber = errNumber;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getNominal() {
        return nominal;
    }

    public void setNominal(String nominal) {
        this.nominal = nominal;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }
}
