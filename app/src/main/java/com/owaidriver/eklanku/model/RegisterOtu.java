package com.owaidriver.eklanku.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Iko Wirya on 4/12/2019.
 */
public class RegisterOtu {
    @SerializedName("userID")
    private String userID;
    @SerializedName("uplineID")
    private String uplineID;
    @SerializedName("aplUse")
    private String aplUse;
    @SerializedName("nama")
    private String nama;
    @SerializedName("email")
    private String email;
    @SerializedName("password")
    private String password;
    @SerializedName("pin")
    private String pin;

    public RegisterOtu(String userID, String uplineID, String aplUse, String nama, String email, String password, String pin) {
        this.aplUse = aplUse;

        this.userID = userID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUplineID() {
        return uplineID;
    }

    public void setUplineID(String uplineID) {
        this.uplineID = uplineID;
    }

    public String getAplUse() {
        return aplUse;
    }

    public void setAplUse(String aplUse) {
        this.aplUse = aplUse;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
}
