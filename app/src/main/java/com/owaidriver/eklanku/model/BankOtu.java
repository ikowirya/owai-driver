package com.owaidriver.eklanku.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Iko Wirya on 4/16/2019.
 */
public class BankOtu {
    @SerializedName("errNumber")
    private String errNumber;
    @SerializedName("userID")
    private String userID;
    @SerializedName("banklist")
    private List<BankListOtu> banklist;
    @SerializedName("respTime")
    private String respTime;
    @SerializedName("status")
    private String status;
    @SerializedName("respMessage")
    private String respMessage;

    public BankOtu(String errNumber, String userID, List<BankListOtu> banklist, String respTime, String status, String respMessage) {
        this.errNumber = errNumber;
        this.userID = userID;
        this.banklist = banklist;
        this.respTime = respTime;
        this.status = status;
        this.respMessage = respMessage;
    }

    public String getErrNumber() {

        return errNumber;
    }

    public void setErrNumber(String errNumber) {
        this.errNumber = errNumber;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public List<BankListOtu> getBanklist() {
        return banklist;
    }

    public void setBanklist(List<BankListOtu> banklist) {
        this.banklist = banklist;
    }

    public String getRespTime() {
        return respTime;
    }

    public void setRespTime(String respTime) {
        this.respTime = respTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }
}
