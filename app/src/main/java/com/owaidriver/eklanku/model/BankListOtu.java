package com.owaidriver.eklanku.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Iko Wirya on 4/16/2019.
 */
public class BankListOtu
{
    @SerializedName("bank")
    private String bank;
    @SerializedName("norec")
    private String norec;
    @SerializedName("anbank")
    private String anbank;
    @SerializedName("isactive")
    private String isactive;
    @SerializedName("logo")
    private String logo;

    public BankListOtu(String bank, String norec, String anbank, String isactive, String logo) {
        this.bank = bank;
        this.norec = norec;
        this.anbank = anbank;
        this.isactive = isactive;
        this.logo = logo;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getNorec() {
        return norec;
    }

    public void setNorec(String norec) {
        this.norec = norec;
    }

    public String getAnbank() {
        return anbank;
    }

    public void setAnbank(String anbank) {
        this.anbank = anbank;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
