package com.owaidriver.eklanku.fragment;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.owaidriver.eklanku.MainActivity;
import com.owaidriver.eklanku.R;
import com.owaidriver.eklanku.database.DBHandler;
import com.owaidriver.eklanku.database.Queries;
import com.owaidriver.eklanku.model.Driver;
import com.owaidriver.eklanku.model.PaymentResponseOtu;
import com.owaidriver.eklanku.model.ResponseTopUp;
import com.owaidriver.eklanku.network.HTTPHelper;
import com.owaidriver.eklanku.network.Log;
import com.owaidriver.eklanku.network.NetworkActionResult;
import com.owaidriver.eklanku.network.OtuApi;
import com.owaidriver.eklanku.network.OtuService;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Callback;

//import net.gumcode.drivermangjek.preference.UserPreference;


public class WithdrawFragment extends Fragment{
    private static final String TAG = WithdrawFragment.class.getSimpleName();
    private View rootView;
    MainActivity activity;
    Driver driver;
    int maxRetry = 4;
    OtuService otuService;
    @BindView(R.id.txtPinOtu)
    EditText txtPinOtu;
    @BindView(R.id.nominalWithdraw)
    EditText nominalWithdraw;
    String userID,mbr_token;
    public WithdrawFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.content_withdraw, container, false);
        ButterKnife.bind(this,rootView);
        activity = (MainActivity) getActivity();
//        activity.getSupportActionBar().setTitle("Withdraw");
        Queries que = new Queries(new DBHandler(activity));
        driver = que.getDriver();
        que.closeDatabase();
        initView();
        otuService = OtuApi.getClient().create(OtuService.class);
        SharedPreferences preferences = getActivity().getSharedPreferences("MyPref",0);
        userID = preferences.getString("userID", "");
        mbr_token = preferences.getString("mbr_token", "");
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    private void initView() {
        TextView butSubmit;
        final String key="";
        butSubmit = rootView.findViewById(R.id.butSubmitW);

        butSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if(driver.no_rek.equals("")){
//                    Toast.makeText(activity, "Please update your account data in the menu settings!", Toast.LENGTH_LONG).show();
//                }else{
//
//                }
                if(nominalWithdraw.getText().toString().equals("")&&txtPinOtu.getText().toString().equals("")){
                    Toast.makeText(activity, "Please complete form withdraw !", Toast.LENGTH_SHORT).show();
                }else{
                    if(Integer.parseInt(nominalWithdraw.getText().toString()) > (driver.deposit - 50000)){
                        Toast.makeText(activity, "The minimum balance required is 50.000.", Toast.LENGTH_LONG).show();
                    }else{
//                        checkKey();
                        withdrawalOtu("OWAIDRIVER",mbr_token,userID,nominalWithdraw.getText().toString(),
                                txtPinOtu.getText().toString(),"1234");
//                            withdrawal(nominalWithdraw.getText().toString());
                    }
                }
            }
        });
    }

    private void checkKey(){
        retrofit2.Call<PaymentResponseOtu> users = otuService.checkKeyOtu(userID,"OWAIDRIVER",mbr_token);
        users.enqueue(new Callback<PaymentResponseOtu>() {
            @Override
            public void onResponse(retrofit2.Call<PaymentResponseOtu> call, retrofit2.Response<PaymentResponseOtu> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals("SUCCESS"))
                    {
                        withdrawalOtu("OWAIDRIVER",mbr_token,userID,nominalWithdraw.getText().toString(),
                                txtPinOtu.getText().toString(),"1234");
//                        android.util.Log.d(TAG, "onResponse 1: "+response.body().getStatus());
                    }
                    else {
                        Snackbar.make(rootView.findViewById(R.id.main), ""+ response.body().getRespMessage() , Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<PaymentResponseOtu> call, Throwable t) {
                android.util.Log.e("TAG", "onFailure: " + t.toString());
                t.printStackTrace();
                Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void withdrawalOtu(String aplUse, String accessToken, String userID, String jumlah, String pin, String key) {
        final ProgressDialog md1 = showLoading();
        retrofit2.Call<PaymentResponseOtu> users = otuService.withdrawOtu("OWAIDRIVER",accessToken,userID,jumlah,pin,"12345");
        users.enqueue(new Callback<PaymentResponseOtu>() {
            @Override
            public void onResponse(retrofit2.Call<PaymentResponseOtu> call, retrofit2.Response<PaymentResponseOtu> response) {
                md1.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals("SUCCESS"))
                    {
                        withdrawal(nominalWithdraw.getText().toString());
//                        Toast.makeText(activity, "Withdrawal Success", Toast.LENGTH_SHORT).show();
//                        android.util.Log.d(TAG, "onResponse 2: "+response.body().getStatus());
                    }
                    else {
                        Snackbar.make(rootView.findViewById(R.id.main), ""+ response.body().getRespMessage() , Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<PaymentResponseOtu> call, Throwable t) {
                md1.dismiss();
                android.util.Log.e("TAG", "onFailure: " + t.toString());
                t.printStackTrace();
                Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private ProgressDialog showLoading(){
        ProgressDialog ad = ProgressDialog.show(activity, "", "Loading...", true);
        return ad;
    }

    private void withdrawal(final String nominal){

        JSONObject jWith = new JSONObject();
        try {
            jWith.put("jumlah", nominal);
            jWith.put("id_driver", driver.id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        Log.d("Update_withdrawal", jWith.toString());
        HTTPHelper.getInstance(activity).withdrawal(jWith, new NetworkActionResult() {
            @Override
            public void onSuccess(JSONObject obj) {
                try {
                    if(obj.getString("message").equals("success")){
//                        Toast.makeText(activity, "Withdrawals will be processed immediately..", Toast.LENGTH_SHORT).show();
                        Toast.makeText(activity, "Withdraw Success", Toast.LENGTH_SHORT).show();

                    }else{
                        Toast.makeText(activity, "Withdraw failed", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                maxRetry = 4;
            }


            @Override
            public void onFailure(String message) {
            }

            @Override
            public void onError(String message) {
                if(maxRetry == 0){
                    Toast.makeText(activity, "Connection problem..", Toast.LENGTH_SHORT).show();
                    maxRetry = 4;
                }else{
                    withdrawal(nominal);
                    maxRetry--;
                    Log.d("Try_ke_withdraw", String.valueOf(maxRetry));

                }
            }
        });
    }
}