package com.owaidriver.eklanku.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.owaidriver.eklanku.MainActivity;
import com.owaidriver.eklanku.R;
import com.owaidriver.eklanku.database.DBHandler;
import com.owaidriver.eklanku.database.Queries;
import com.owaidriver.eklanku.model.BarangBelanja;
import com.owaidriver.eklanku.model.DestinasiMbox;
import com.owaidriver.eklanku.model.Driver;
import com.owaidriver.eklanku.model.MakananBelanja;
import com.owaidriver.eklanku.model.ResponseCheckOtu;
import com.owaidriver.eklanku.model.ResponseRegisterOtu;
import com.owaidriver.eklanku.model.SaldoBonusOtu;
import com.owaidriver.eklanku.model.Transaksi;
import com.owaidriver.eklanku.network.HTTPHelper;
import com.owaidriver.eklanku.network.Log;
import com.owaidriver.eklanku.network.NetworkActionResult;
import com.owaidriver.eklanku.network.OtuApi;
import com.owaidriver.eklanku.network.OtuService;
import com.owaidriver.eklanku.service.LocationService;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Callback;

//import net.gumcode.drivermangjek.preference.UserPreference;


public class DashboardFragment extends Fragment {
    private static final String TAG = DashboardFragment.class.getSimpleName();
    private View rootView;
    MainActivity activity;
    ImageView imageBekerja,bg_work;
    Switch switchBekerja;
    ImageView imgStatus;
    LinearLayout content_main;
    FrameLayout switchWrapper;
    boolean isOn = false;
    Driver driver;
    int maxRetry = 4;
    int maxRetry1 = 4;
    int maxRetry2 = 4;
    ProgressDialog pd, pd1;
    OtuService otuService;
    public DashboardFragment(){
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.content_main, container, false);
        setHasOptionsMenu(true);

        imageBekerja = rootView.findViewById(R.id.iconBekerja);
        content_main = rootView.findViewById(R.id.content_main);
        imgStatus = rootView.findViewById(R.id.imgStatus);
        bg_work = rootView.findViewById(R.id.bg_work);
        switchBekerja = rootView.findViewById(R.id.switch_bekerja);
        switchWrapper = rootView.findViewById(R.id.switch_wrapper);
        activity = (MainActivity) getActivity();
//        activity.getSupportActionBar().setTitle("Owai Driver");
        otuService = OtuApi.getClient().create(OtuService.class);
        Queries quem = new Queries(new DBHandler(activity));
        driver = quem.getDriver();
//        android.util.Log.d(TAG, "onCreateView: "+driver.status);
        quem.closeDatabase();
        activate();
        pd = showLoading();

        syncronizingAccount();
        switchWrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Queries que = new Queries(new DBHandler(activity));
                Driver driver = que.getDriver();
                que.closeDatabase();
                if(driver.status == 4){
                    pd1 = showLoading();
                    turningTheJob(true);
                }else{
                    showWarning();
                }
            }
        });
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        que.closeDatabase();
    }

    private MaterialDialog showWarning(){
        final MaterialDialog md  = new  MaterialDialog.Builder(activity)
                .title("WARNING")
                .content("Sure you want to stop getting orders?")
                .icon(new IconicsDrawable(activity)
                        .icon(FontAwesome.Icon.faw_exclamation_triangle)
                        .color(Color.RED)
                        .sizeDp(24))
                .positiveText("Yes")
                .negativeText("Cancel")
                .positiveColor(Color.BLUE)
                .negativeColor(Color.DKGRAY)
                .show();

        View positive = md.getActionButton(DialogAction.POSITIVE);
        View negative = md.getActionButton(DialogAction.NEGATIVE);

        positive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd1 = showLoading();
                turningTheJob(false);
                md.dismiss();
            }
        });
        negative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                md.dismiss();
            }
        });

        return md;
    }

    private MaterialDialog showMessage(String title, String message){
        final MaterialDialog md  = new  MaterialDialog.Builder(activity)
                .title(title)
                .content(message)
                .icon(new IconicsDrawable(activity)
                        .icon(FontAwesome.Icon.faw_exclamation_triangle)
                        .color(Color.GREEN)
                        .sizeDp(24))
                .positiveText("Close")
                .positiveColor(Color.DKGRAY)
                .show();

        View positive = md.getActionButton(DialogAction.POSITIVE);
//        View negative = md.getActionButton(DialogAction.NEGATIVE);

        positive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                md.dismiss();
            }
        });

        return md;
    }

    public void activate(){
        Queries que = new Queries(new DBHandler(activity));
        Driver driver = que.getDriver();
        que.closeDatabase();
        if(driver.status == 4){
            switchBekerja.setChecked(false);
            imgStatus.setBackgroundResource(R.drawable.rest);
            bg_work.setImageResource(R.drawable.offline_work);
            imageBekerja.setImageResource(R.drawable.offline);
            content_main.setBackgroundResource(R.drawable.gradient_offline);

        }else{
            switchBekerja.setChecked(true);
            imgStatus.setBackgroundResource(R.drawable.happy);
            bg_work.setImageResource(R.drawable.online_work);
            imageBekerja.setImageResource(R.drawable.online);
            content_main.setBackgroundResource(R.drawable.gradient_online);

        }
    }

    private void turningOff(){
        Queries que = new Queries(new DBHandler(activity));

//        SettingPreference sp = new SettingPreference(activity);
        switchBekerja.setChecked(false);
        imgStatus.setBackgroundResource(R.drawable.rest);
        bg_work.setImageResource(R.drawable.offline_work);
        imageBekerja.setImageResource(R.drawable.offline);
        content_main.setBackgroundResource(R.drawable.gradient_offline);

//        sp.updateKerja("OFF");
        que.updateStatus(4);
        que.closeDatabase();
    }

    private void turningOn(){
        Queries que = new Queries(new DBHandler(activity));

//        SettingPreference sp = new SettingPreference(activity);
        switchBekerja.setChecked(true);
        imgStatus.setBackgroundResource(R.drawable.happy);
        bg_work.setImageResource(R.drawable.online_work);
        imageBekerja.setImageResource(R.drawable.online);
        content_main.setBackgroundResource(R.drawable.gradient_online);

        que.updateStatus(1);
        que.closeDatabase();

//        sp.updateKerja("ON");
        Intent service = new Intent(activity, LocationService.class);
        activity.startService(service);
    }

    private void turningTheJob(final boolean action){
        JSONObject jTurn = new JSONObject();
        try {
            jTurn.put("is_turn", action);
            jTurn.put("id_driver", driver.id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("JSON_turning_on", jTurn.toString());
        HTTPHelper.getInstance(activity).turningOn(jTurn, new NetworkActionResult() {
            @Override
            public void onSuccess(JSONObject obj) {
//                android.util.Log.d("DashboardFragment", "onSuccess: " + obj);
                pd1.dismiss();
                maxRetry1 = 4;
                try {
                    if(obj.getString("message").equals("banned")){
                        showMessage("Sorry", "Your account is currently suspended, please contact our office immediately!");
                        turningOff();
                    }else if(obj.getString("message").equals("success")){
                        if(action){
                            turningOn();
                            showMessage("Thank You", "Happy Working Again!");
                        }else{
                            turningOff();
                        }
                    }else{
                        activate();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String message) {
            }

            @Override
            public void onError(String message) {
                if(maxRetry1 == 0){
                    showMessage("Sorry", "A network error has occurred, please try again!");
                    pd1.dismiss();
                    maxRetry1 = 4;
                }else{
//                    pd = showLoading();
                    turningTheJob(action);
                    Log.d("try_ke_turn_off", String.valueOf(maxRetry1));
                    maxRetry1--;
                }
            }
        });
    }

    private ProgressDialog showLoading(){
        ProgressDialog ad = ProgressDialog.show(activity, "", "Loading...", true);
        return ad;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            pd = showLoading();
            pd.setCancelable(false);
            syncronizingAccount();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void syncronizingAccount(){
        //get saldo
        //get status account
        //get get order

        JSONObject jSync = new JSONObject();
        try {
            jSync.put("id", driver.id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HTTPHelper.getInstance(activity).syncAccount(jSync, new NetworkActionResult() {
            @Override
            public void onSuccess(JSONObject obj) {
                try {
                    if(obj.getString("message").equals("success")){

                        driver = HTTPHelper.getInstance(activity).parseUserSync(activity, obj.toString());
                        activity.textRating.setText(convertJarak(Double.parseDouble(driver.rating))+" / 5");
                        Queries que = new Queries(new DBHandler(activity));
                        que.updateRating(driver.rating);
                        que.updateStatus(driver.status);
                        que.closeDatabase();
//                        android.util.Log.d("appa", "onSuccess: "+driver.status);
                        Transaksi runTrans = HTTPHelper.getInstance(activity).parseTransaksi(activity, obj.toString());
                        if(!runTrans.id_transaksi.equals("0")){
                            selectRunTrans(runTrans);
                        }

                        SharedPreferences preferences = getActivity().getSharedPreferences("MyPref",0);
                        String userID = preferences.getString("userID", "");
                        String mbr_token = preferences.getString("mbr_token", "");
                        getSaldoBonus(userID,mbr_token,"OWAIDRIVER");
//                        android.util.Log.d(TAG, "hasilkuu: "+userID+mbr_token);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                pd.dismiss();
                maxRetry = 4;
            }

            @Override
            public void onFailure(String message) {
            }

            @Override
            public void onError(String message) {
//                pd.dismiss();
                if(maxRetry == 0){
                    Toast.makeText(activity, "Connection is problem..", Toast.LENGTH_SHORT).show();
                    pd.dismiss();
                    maxRetry = 4;
                }else{
//                    pd = showLoading();
                    syncronizingAccount();
                    Log.d("try_ke_sync ", String.valueOf(maxRetry));
                    maxRetry--;
                }

            }
        });
    }

    private void getSaldoBonus(String userID, String accessToken, String aplUse){
        retrofit2.Call<SaldoBonusOtu> users = otuService.saldoBonus(userID,accessToken,aplUse);
        users.enqueue(new Callback<SaldoBonusOtu>() {
            @Override
            public void onResponse(retrofit2.Call<SaldoBonusOtu> call, retrofit2.Response<SaldoBonusOtu> response) {
                if (response.isSuccessful()) {

                    android.util.Log.d(TAG, "onResponse: "+response.body().getStatus());
                    if (response.body().getStatus().equals("SUCCESS")){
                        double saldo = response.body().getBalance().get(0).getSaldo_driver();
                        activity.saldo.setText(amountAdapter(saldo));
                        Queries que = new Queries(new DBHandler(activity));
                        que.updateDeposit(saldo);
                        que.closeDatabase();
                    }

                    else {

                    }
                } else {
                    Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<SaldoBonusOtu> call, Throwable t) {
                android.util.Log.e("TAG", "onFailure: " + t.toString());
                t.printStackTrace();
                Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String convertJarak(Double jarak){
        int range = (int)(jarak*10);
        jarak = (double)range/10;
        return String.valueOf(jarak);
    }

    private String amountAdapter(double amo){
        return "Rp "+ NumberFormat.getNumberInstance(Locale.GERMANY).format(amo);
    }

    private void selectRunTrans(Transaksi runTruns){
        JSONObject jTrans = new JSONObject();
        try {
            jTrans.put("id_transaksi", runTruns.id_transaksi);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        switch (runTruns.order_fitur){
            case "3":{
                get_data_transaksi_mfood(jTrans, runTruns);
                break;
            }
            case "4":{
                get_data_transaksi_mmart(jTrans, runTruns);
                break;
            }
            case "5":{
                get_data_transaksi_msend(jTrans, runTruns);
                break;
            }
            case "6":{
                get_data_transaksi_mmassage(jTrans, runTruns);
                break;
            }
            case "7":{
                get_data_transaksi_mbox(jTrans, runTruns);
                break;
            }
            case "8":{
                get_data_transaksi_mservice(jTrans, runTruns);
                break;
            }
            default:{
                Queries que = new Queries(new DBHandler(activity));
                que.truncate(DBHandler.TABLE_IN_PROGRESS_TRANSAKSI);
                que.insertInProgressTransaksi(runTruns);
                que.closeDatabase();
                changeFragment(new OrderFragment(), false);
                break;
            }
        }
    }

    private void get_data_transaksi_mmart(final JSONObject jTrans, final Transaksi currTrans){
        final ProgressDialog pd = showLoading();
        HTTPHelper.getInstance(activity).getTransaksiMmart(jTrans, new NetworkActionResult() {
            @Override
            public void onSuccess(JSONObject obj) {
                Transaksi transaksi = HTTPHelper.parseDataMmart(currTrans, obj);
                ArrayList<BarangBelanja> arrBarang = HTTPHelper.parseBarangBelanja(obj);
                Queries que = new Queries(new DBHandler(activity));
                que.truncate(DBHandler.TABLE_IN_PROGRESS_TRANSAKSI);
                que.insertInProgressTransaksi(transaksi);
                que.truncate(DBHandler.TABLE_BARANG_BELANJA);
                que.insertBarangBelanja(arrBarang);
                que.closeDatabase();
                changeFragment(new OrderFragment(), false);
                pd.dismiss();
                maxRetry2 = 4;
            }

            @Override
            public void onFailure(String message) {
            }

            @Override
            public void onError(String message) {
                if(maxRetry2 == 0){
                    pd.dismiss();
                    Toast.makeText(activity, "Connection is problem..", Toast.LENGTH_SHORT).show();
                    Log.d("data_sync_mmart", "Retrieving Data Null");
                    maxRetry2 = 4;
                }else{
                    get_data_transaksi_mmart(jTrans, currTrans);
                    maxRetry2--;
                    Log.d("Try_ke_data_mmart", String.valueOf(maxRetry2));
                    pd.dismiss();
                }
            }
        });
    }

    private void get_data_transaksi_mfood(final JSONObject jTrans, final Transaksi currTrans){
        final ProgressDialog pd = showLoading();
        HTTPHelper.getInstance(activity).getTransaksiMfood(jTrans, new NetworkActionResult() {
            @Override
            public void onSuccess(JSONObject obj) {
                Transaksi transaksi = HTTPHelper.parseDataMmart(currTrans, obj);
                ArrayList<MakananBelanja> arrBarang = HTTPHelper.parseMakananBelanja(obj);
                Queries que = new Queries(new DBHandler(activity));
                que.truncate(DBHandler.TABLE_IN_PROGRESS_TRANSAKSI);
                que.insertInProgressTransaksi(transaksi);
                que.truncate(DBHandler.TABLE_MAKANAN_BELANJA);
                que.insertMakananBelanja(arrBarang);
                que.closeDatabase();
                changeFragment(new OrderFragment(), false);
                pd.dismiss();
                maxRetry2 = 4;
            }

            @Override
            public void onFailure(String message) {
            }

            @Override
            public void onError(String message) {
                if(maxRetry2 == 0){
                    pd.dismiss();
                    Toast.makeText(activity, "Connection is problem..", Toast.LENGTH_SHORT).show();
                    Log.d("data_sync_mfood", "Retrieving Data Null");
                    maxRetry2 = 4;
                }else{
                    get_data_transaksi_mfood(jTrans, currTrans);
                    maxRetry2--;
                    Log.d("Try_ke_data_mfood", String.valueOf(maxRetry2));
                    pd.dismiss();
                }
            }
        });
    }

    private void get_data_transaksi_mbox(final JSONObject jTrans, final Transaksi currTrans){
        final ProgressDialog pd = showLoading();
        HTTPHelper.getInstance(activity).getTransaksiMbox(jTrans, new NetworkActionResult() {
            @Override
            public void onSuccess(JSONObject obj) {
                Transaksi transaksi = HTTPHelper.parseDataMbox(currTrans, obj);
                ArrayList<DestinasiMbox> arrDestinasi = HTTPHelper.parseDestinasiMbox(obj);
                Queries que = new Queries(new DBHandler(activity));
                que.truncate(DBHandler.TABLE_IN_PROGRESS_TRANSAKSI);
                que.insertInProgressTransaksi(transaksi);
                que.truncate(DBHandler.TABLE_DESTINASI_MBOX);
                que.insertDestinasiMbox(arrDestinasi);
                que.closeDatabase();
                changeFragment(new OrderFragment(), false);
                pd.dismiss();
                maxRetry2 = 4;
            }

            @Override
            public void onFailure(String message) {
            }

            @Override
            public void onError(String message) {
                if(maxRetry2 == 0){
                    pd.dismiss();
                    Toast.makeText(activity, "Connection is problem..", Toast.LENGTH_SHORT).show();
                    Log.d("data_sync_mbox", "Retrieving Data Null");
                    maxRetry2 = 4;
                }else{
                    get_data_transaksi_mbox(jTrans, currTrans);
                    maxRetry2--;
                    Log.d("Try_ke_data_mbox", String.valueOf(maxRetry2));
                    pd.dismiss();
                }
            }
        });
    }

    private void get_data_transaksi_msend(final JSONObject jTrans, final Transaksi currTrans){
        final ProgressDialog pd = showLoading();
        HTTPHelper.getInstance(activity).getTransaksiMsend(jTrans, new NetworkActionResult() {
            @Override
            public void onSuccess(JSONObject obj) {
                Transaksi transaksi = HTTPHelper.parseDataMsend(currTrans, obj);
                Queries que = new Queries(new DBHandler(activity));
                que.truncate(DBHandler.TABLE_IN_PROGRESS_TRANSAKSI);
                que.insertInProgressTransaksi(transaksi);
                que.closeDatabase();
                changeFragment(new OrderFragment(), false);
                pd.dismiss();
                maxRetry2 = 4;
            }

            @Override
            public void onFailure(String message) {
            }

            @Override
            public void onError(String message) {
                if(maxRetry2 == 0){
                    pd.dismiss();
                    Toast.makeText(activity, "Connection is problem..", Toast.LENGTH_SHORT).show();
                    Log.d("data_sync_msend", "Retrieving Data Null");
                    maxRetry2 = 4;
                }else{
                    get_data_transaksi_msend(jTrans, currTrans);
                    maxRetry2--;
                    Log.d("Try_ke_data_msend", String.valueOf(maxRetry2));
                    pd.dismiss();
                }
            }
        });
    }

    private void get_data_transaksi_mservice(final JSONObject jTrans, final Transaksi currTrans){
        final ProgressDialog pd = showLoading();
        HTTPHelper.getInstance(activity).getTransaksiMservice(jTrans, new NetworkActionResult() {
            @Override
            public void onSuccess(JSONObject obj) {
                Transaksi transaksi = HTTPHelper.parseDataMservice(currTrans, obj);
                Queries que = new Queries(new DBHandler(activity));
                que.truncate(DBHandler.TABLE_IN_PROGRESS_TRANSAKSI);
                que.insertInProgressTransaksi(transaksi);
                que.closeDatabase();
                changeFragment(new OrderFragment(), false);
                pd.dismiss();
                maxRetry2 = 4;
            }

            @Override
            public void onFailure(String message) {
            }

            @Override
            public void onError(String message) {
                if(maxRetry2 == 0){
                    pd.dismiss();
                    Toast.makeText(activity, "Connection is problem..", Toast.LENGTH_SHORT).show();
                    Log.d("data_sync_mservice", "Retrieving Data Null");
                    maxRetry2 = 4;
                }else{
                    get_data_transaksi_mservice(jTrans, currTrans);
                    maxRetry2--;
                    Log.d("Try_ke_data_mservice", String.valueOf(maxRetry2));
                    pd.dismiss();
                }
            }
        });
    }

    private void get_data_transaksi_mmassage(final JSONObject jTrans, final Transaksi currTrans){
        final ProgressDialog pd = showLoading();
        HTTPHelper.getInstance(activity).getTransaksiMmassage(jTrans, new NetworkActionResult() {
            @Override
            public void onSuccess(JSONObject obj) {
                Transaksi transaksi = HTTPHelper.parseDataMmassage(currTrans, obj);
                Queries que = new Queries(new DBHandler(activity));
                que.truncate(DBHandler.TABLE_IN_PROGRESS_TRANSAKSI);
                que.insertInProgressTransaksi(transaksi);
                que.closeDatabase();
                changeFragment(new OrderFragment(), false);
                pd.dismiss();
                maxRetry2 = 4;
            }

            @Override
            public void onFailure(String message) {
            }

            @Override
            public void onError(String message) {
                if(maxRetry2 == 0){
                    pd.dismiss();
                    Toast.makeText(activity, "Connection is problem..", Toast.LENGTH_SHORT).show();
                    Log.d("data_sync_mmassage", "Retrieving Data Null");
                    maxRetry2 = 4;
                }else{
                    get_data_transaksi_mmassage(jTrans, currTrans);
                    maxRetry2--;
                    Log.d("Try_ke_data_mmassage", String.valueOf(maxRetry2));
                    pd.dismiss();
                }
            }
        });
    }

    public void changeFragment(Fragment frag, boolean addToBackStack) {
        FragmentManager fm = activity.getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.replace(R.id.container_body, frag);
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }


}
