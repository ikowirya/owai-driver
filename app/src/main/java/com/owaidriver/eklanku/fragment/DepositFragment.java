package com.owaidriver.eklanku.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.owaidriver.eklanku.MainActivity;
import com.owaidriver.eklanku.R;
import com.owaidriver.eklanku.activity.LoginOtuActivity;
import com.owaidriver.eklanku.adapter.BankAdapter;
import com.owaidriver.eklanku.database.DBHandler;
import com.owaidriver.eklanku.database.Queries;
import com.owaidriver.eklanku.model.BankListOtu;
import com.owaidriver.eklanku.model.BankOtu;
import com.owaidriver.eklanku.model.Driver;
import com.owaidriver.eklanku.model.ResponseCheckOtu;
import com.owaidriver.eklanku.model.ResponseRegisterOtu;
import com.owaidriver.eklanku.model.ResponseTopUp;
import com.owaidriver.eklanku.network.HTTPHelper;
import com.owaidriver.eklanku.network.Log;
import com.owaidriver.eklanku.network.NetworkActionResult;
import com.owaidriver.eklanku.network.OtuApi;
import com.owaidriver.eklanku.network.OtuService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Callback;

import static android.app.Activity.RESULT_OK;
import static android.os.Build.VERSION_CODES.N;

//import net.gumcode.drivermangjek.preference.UserPreference;


public class DepositFragment extends Fragment{
    private static final String TAG = DepositFragment.class.getSimpleName();
    private View rootView;
    MainActivity activity;

    private static final int TAKE_PICTURE = 1;
    private Uri imageUri;
//    TextView upload;
    String bukti = "";
    Driver driver;
    private File file;
    private byte[] bytes;
    RecyclerView rvData;
    int maxRetry = 4;

//    EditText nama, norek, jumlah;
    EditText jumlah;
    RelativeLayout main;
    RecyclerView.LayoutManager mLayoutManager;
    BankAdapter bankAdapter;
    OtuService otuService;
    Spinner spinBank;
    String valueJumlah;
    public DepositFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.content_deposit, container, false);

        activity = (MainActivity) getActivity();
//        activity.getSupportActionBar().setTitle("Deposit");
        Queries que = new Queries(new DBHandler(activity));
        driver = que.getDriver();
        que.closeDatabase();
        initView();
        otuService = OtuApi.getClient().create(OtuService.class);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rvData.setLayoutManager(mLayoutManager);
        final SharedPreferences preferences = getActivity().getSharedPreferences("MyPref",0);
        final String userID = preferences.getString("userID", "");
        final String mbr_token = preferences.getString("mbr_token", "");
        getBank(userID,mbr_token,"OWAIDRIVER");
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//            upload.setEnabled(false);
            ActivityCompat.requestPermissions(activity, new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
        }
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    String bankName = "";
    private void initView() {
        final TextView topup;




//        nama = rootView.findViewById(R.id.pemilikRekening);
//        norek = rootView.findViewById(R.id.nomorRekening);
        rvData = rootView.findViewById(R.id.rvData);
        jumlah = rootView.findViewById(R.id.nominalTransfer);
        main = rootView.findViewById(R.id.main);
        spinBank = rootView.findViewById(R.id.spinBank);
        final SharedPreferences preferences = getActivity().getSharedPreferences("MyPref",0);
        final String userID = preferences.getString("userID", "");
        final String mbr_token = preferences.getString("mbr_token", "");
        final String token = preferences.getString("token", "");


//        // Array of choices
//        String colors[] = {"Red","Blue","White","Yellow","Black", "Green","Purple","Orange","Grey"};
//
//
//    // Application of the Array to the Spinner
//        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(),
//                android.R.layout.simple_spinner_item, catrgoryList);
//        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
//        spinBank.setAdapter(spinnerArrayAdapter);




//        upload = rootView.findViewById(R.id.butUploadBukti);
        topup = rootView.findViewById(R.id.butTopup);

        topup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valueJumlah =  jumlah.getText().toString();
                if (!valueJumlah.isEmpty()||!bankName.isEmpty()){
                    topupOtu(userID, mbr_token, "OWAIDRIVER", bankName,valueJumlah);
                }else {
                    Snackbar.make(rootView.findViewById(R.id.main), "Please, Completed Form Data!", Snackbar.LENGTH_SHORT).show();
                }

            }
        });

//        upload.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                takePicture();
//            }
//        });

//        topup.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(cekIsFill(nama, norek, jumlah)){
//
//
//                    JSONObject jVer = new JSONObject();
//                    try {
//                        jVer.put("id", driver.id);
//                        jVer.put("no_rekening", norek.getText().toString());
//                        jVer.put("jumlah", jumlah.getText().toString());
//                        jVer.put("atas_nama", nama.getText().toString());
//                        jVer.put("bank", bankName);
//                        jVer.put("bukti", bukti);
//
//                        verifikasiTopup(jVer);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }else{
//
//                }
//            }
//        });
    }

    private void topupOtu(String userID, String accessToken, String aplUse, String bank, String nominal) {
        retrofit2.Call<ResponseTopUp> users = otuService.requestBank(userID,accessToken,aplUse,bank,nominal);
        users.enqueue(new Callback<ResponseTopUp>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseTopUp> call, retrofit2.Response<ResponseTopUp> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals("SUCCESS"))
                    {
                        valueJumlah = response.body().getNominal();
                        updateSaldoOtuOwai();
                    }
                    else {
                        Snackbar.make(rootView.findViewById(R.id.main), ""+ response.body().getRespMessage() , Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseTopUp> call, Throwable t) {
                android.util.Log.e("TAG", "onFailure: " + t.toString());
                t.printStackTrace();
                Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateSaldoOtuOwai(){
        final JSONObject logData = new JSONObject();
        try {
//            logData.put("email", email.getText().toString());
            logData.put("nominal", valueJumlah);
            logData.put("id_driver", driver.id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HTTPHelper.getInstance(activity).updateAccessTokenOtu(logData, new NetworkActionResult() {
            @Override
            public void onSuccess(JSONObject obj) {
//                Log.d("hayo", obj.toString());
                try {
                    if(obj.getString("message").equals("success")) {
                        Snackbar.make(rootView.findViewById(R.id.main), "TopUp Successful", Snackbar.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(activity, "Failed!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                maxRetry = 4;
            }

            @Override
            public void onFailure(String message) {

            }

            @Override
            public void onError(String message) {
//                Log.d("Pesan_error", message);
                if(maxRetry == 0){
                    showWarning();
                    maxRetry = 4;
                }else{
                    updateSaldoOtuOwai();
                    maxRetry--;
                    Log.d("Try_ke_login", String.valueOf(maxRetry));
                }

            }
        });
    }

//    private boolean cekIsFill(EditText nama, EditText norek, EditText jumlah){
//        boolean isFill = true;
//        if(nama.getText().toString().equals("")){
//            isFill = false;
//        }
//        if(norek.getText().toString().equals("")){
//            isFill = false;
//        }
//        if(jumlah.getText().toString().equals("")){
//            isFill = false;
//        }
//        if (bukti.equals("")){
//            isFill = false;
//            Toast.makeText(activity, "Enter proof of payment", Toast.LENGTH_SHORT).show();
//        }
//        return isFill;
//    }

    private void getBank(String userID, String accessToken, String aplUse ){
        retrofit2.Call<BankOtu> users = otuService.getBank(userID,accessToken,aplUse);
        users.enqueue(new Callback<BankOtu>() {
            @Override
            public void onResponse(retrofit2.Call<BankOtu> call, retrofit2.Response<BankOtu> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals("SUCCESS"))
                    {
                        List<String> list = new ArrayList<String>(); // List of Items
                        List<BankListOtu> bankListOtus= response.body().getBanklist();
                        for (int i = 0; i < bankListOtus.size(); i++) {
                            if (bankListOtus.size() > 0){
                                list.add(bankListOtus.get(i).getBank());
                            }
                        }
                        spinBank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                bankName =  adapterView.getItemAtPosition(i).toString();

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });

                        ArrayAdapter<String> SpinnerAdapter = new ArrayAdapter<String>
                                (getActivity(), android.R.layout.simple_spinner_item, list){
                            //By using this method we will define how
                            // the text appears before clicking a spinner
                            public View getView(int position, View convertView,
                                                ViewGroup parent) {
                                View v = super.getView(position, convertView, parent);
                                ((TextView) v).setTextColor(Color.parseColor("#f7b914"));
                                return v;
                            }
                            //By using this method we will define
                            //how the listview appears after clicking a spinner
                            public View getDropDownView(int position, View convertView,
                                                        ViewGroup parent) {
                                View v = super.getDropDownView(position, convertView,
                                        parent);
                                v.setBackgroundColor(Color.parseColor("#f7b914"));
                                ((TextView) v).setTextColor(Color.parseColor("#ffffff"));
                                return v;
                            }
                        };
                        SpinnerAdapter.setDropDownViewResource(
                                android.R.layout.simple_spinner_dropdown_item);
                        // Set Adapter in the spinner
                        spinBank.setAdapter(SpinnerAdapter);


                        bankAdapter = new BankAdapter(bankListOtus);
                        rvData.setAdapter(bankAdapter);

                    }

                    else {
                    }
                } else {
                    Toast.makeText(getContext(), "Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<BankOtu> call, Throwable t) {
                android.util.Log.e("TAG", "onFailure: " + t.toString());
                t.printStackTrace();
                Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
//                upload.setEnabled(true);
            }
        }
    }

    public void changeFragment(Fragment frag, boolean addToBackStack) {
        FragmentManager fm = activity.getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.container_body, frag);
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    private ProgressDialog showLoading(){
        ProgressDialog ad = ProgressDialog.show(activity, "", "Loading...", true);
        return ad;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        switch (requestCode) {
//            case TAKE_PICTURE:
//                if (resultCode == Activity.RESULT_OK) {
////                    MimeTypeMap mime = MimeTypeMap.getSingleton();
////                    String ext = newFile.getName().substring(newFile.getName().lastIndexOf(".") + 1);
////                    String type = mime.getMimeTypeFromExtension(ext);
//                    activity.getContentResolver().notifyChange(imageUri, null);
//                    ContentResolver cr = activity.getContentResolver();
//                    Bitmap bitmap;
//                    try {
////                        Uri selectedImage = data.getData();
////                        String[] filePathColumn = { MediaStore.Images.Media.DATA };
////                                android.provider.MediaStore.Images.Media.getBitmap(cr, imageUri);
////                        Cursor cursor = cr.query(imageUri,
////                                filePathColumn, null, null, null);
////                        cursor.moveToFirst();
////                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
////                        String imgDecodableString = cursor.getString(columnIndex);
//                        bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, imageUri);
//                        Bitmap bm = BitmapFactory.decodeFile(file.getPath());
////                        bitmap = decodeFile(imgDecodableString, 200);
////                        Log.d("after_comppres", String.valueOf(bitmap.getByteCount()));
//                        bukti = compressJSON(bitmap);
//                        if(!bukti.equals("")){
//                            ImageView centang = (ImageView) activity.findViewById(R.id.centang);
//                            centang.setVisibility(View.VISIBLE);
//
//                        }
//
//                    } catch (Exception e) {
//                        Toast.makeText(activity, "Failed to load", Toast.LENGTH_SHORT).show();
////                        Log.e("Camera", e.toString());
//                    }
//                }
//                break;
//            default:
//                break;
//        }
//        if (requestCode == 1) {
//            if (resultCode == RESULT_OK) {
////                File newf = new File(file.getPath());
////                Uri uri = Uri.fromFile(newf);
//
//                Bitmap bm = BitmapFactory.decodeFile(file.getPath());
////                imgProfil.setImageBitmap(bm);
//                bytes = convertBitmapToByteArray(bm);
//                bukti = "data:image/jpeg;base64,"+ Base64.encodeToString(bytes, Base64.DEFAULT);
//                if(!bukti.equals("")){
//                            ImageView centang = activity.findViewById(R.id.centang);
//                            centang.setVisibility(View.VISIBLE);
//
//                        }
//
//            } else {
//                file.delete();
//            }
//        }

    }

    private byte[] convertBitmapToByteArray(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        return baos.toByteArray();
    }


    private void takePicture() {
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File newdir = new File(dir.getPath() + "/public/");
        newdir.mkdir();

        file = new File(newdir + "/" + System.currentTimeMillis() + ".jpg");

        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Uri uri;
        Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (Build.VERSION.SDK_INT > N) {
            uri = FileProvider.getUriForFile(getContext(), getContext().getPackageName() + ".fileProvider", file);
            intentCamera.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else {
            uri = Uri.fromFile(file);
        }
        intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intentCamera,1);
    }


    public void takePhoto() {
//        MimeTypeMap mime = MimeTypeMap.getSingleton();
//        String ext = newFile.getName().substring(newFile.getName().lastIndexOf(".") + 1);
//        String type = mime.getMimeTypeFromExtension(ext);

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "Invoice_" + timeStamp + ".jpg";
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = new File(Environment.getExternalStorageDirectory(), imageFileName);


//        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            imageUri = FileProvider.getUriForFile(getContext(), "com.owaidriver.eklanku.fileProvider", photo);
//        }else{
//            imageUri = Uri.fromFile(photo);
//        }
//        startActivityForResult(intent, TAKE_PICTURE);

        if (Build.VERSION.SDK_INT < N) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(photo));
            imageUri = Uri.fromFile(photo);
        } else {
            File file = new File(photo.getPath());
            Uri photoUri = FileProvider.getUriForFile(getContext(), getContext().getPackageName() + ".fileProvider", file);
            imageUri = photoUri;
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        }
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(intent, TAKE_PICTURE);
    }

    private Bitmap decodeFile(final String path, final int thumbnailSize) {
        Bitmap bitmap;
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, o);
        if ((o.outWidth == -1) || (o.outHeight == -1)) {
            bitmap = null;
        }

        int originalSize = (o.outHeight > o.outWidth) ? o.outHeight
                : o.outWidth;
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = originalSize / thumbnailSize;
        bitmap = BitmapFactory.decodeFile(path, opts);
        return bitmap;
    }


    public String compressJSON(Bitmap bmp){
        byte[] imageBytes0;
        ByteArrayOutputStream baos0 = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG ,50, baos0);
        imageBytes0 = baos0.toByteArray();
        String encodedImage= Base64.encodeToString(imageBytes0, Base64.DEFAULT);
        return encodedImage;
    }

    private void verifikasiTopup(final JSONObject jVer){

        final ProgressDialog pd= showLoading();

        HTTPHelper.getInstance(activity).verifikasiTopUp(jVer, new NetworkActionResult() {
            @Override
            public void onSuccess(JSONObject obj) {
                try {
                    if(obj.getString("message").equals("success")){
                        Toast.makeText(activity, "Thanks. Verification will be processed immediately..", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(activity, "Verification failed..", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                pd.dismiss();
                maxRetry = 4;
            }

            @Override
            public void onFailure(String message) {

            }

            @Override
            public void onError(String message) {
                if(maxRetry == 0){
                    pd.dismiss();
                    showWarning();
                    maxRetry = 4;
                }else{
                    verifikasiTopup(jVer);
                    maxRetry--;
                    Log.d("Try_ke_verifikasi", String.valueOf(maxRetry));
                    pd.dismiss();
                }

            }
        });

    }



    private MaterialDialog showWarning() {
        final MaterialDialog md = new MaterialDialog.Builder(activity)
                .title("Connection problem")
                .content("Try Again!")
                .icon(new IconicsDrawable(activity)
                        .icon(FontAwesome.Icon.faw_exclamation_triangle)
                        .color(Color.YELLOW)
                        .sizeDp(24))
                .positiveText("Close")
                .positiveColor(Color.DKGRAY)
                .show();

        View positive = md.getActionButton(DialogAction.POSITIVE);

        positive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                md.dismiss();
            }
        });
        return md;
    }

}
