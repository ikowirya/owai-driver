package com.owaidriver.eklanku.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.effect.EffectUpdateListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.owaidriver.eklanku.MainActivity;
import com.owaidriver.eklanku.R;
import com.owaidriver.eklanku.activity.LoginOtuActivity;
import com.owaidriver.eklanku.model.RegisterOtu;
import com.owaidriver.eklanku.model.ResponseRegisterOtu;
import com.owaidriver.eklanku.network.OtuApi;
import com.owaidriver.eklanku.network.OtuService;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Callback;

/**
 * Created by Iko Wirya on 4/12/2019.
 */
public class RegisterOtuFragment extends Fragment {
    private View rootView;
    MainActivity activity;
    @BindView(R.id.btnRegisterOtu)
    Button btnRegisterOtu;
    @BindView(R.id.btnLogin)
    LinearLayout btnLogin;
    @BindView(R.id.txtNama)
    EditText txtNama;
    @BindView(R.id.txtPassword)
    EditText txtPassword;
    @BindView(R.id.txtPin)
    EditText txtPin;
    @BindView(R.id.txtUplineID)
    EditText txtUplineID;
    @BindView(R.id.txtUserID)
    EditText txtUserID;
    @BindView(R.id.txtEmail)
    EditText txtEmail;
    @BindView(R.id.btnLogout)
    Button btnLogout;
    @BindView(R.id.registerForm)
    LinearLayout registerForm;
    @BindView(R.id.logoutForm)
    LinearLayout logoutForm;
    String valueNama,valuePassword,valuePin,valueUplineID,valueUserID,valueEmail;
    OtuService otuService;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_register_otu, container, false);
        activity = (MainActivity) getActivity();
        otuService = OtuApi.getClient().create(OtuService.class);
        ButterKnife.bind(this,rootView);
        initView();
        return rootView;
    }

    private void initView() {

        final SharedPreferences preferences = getActivity().getSharedPreferences("MyPref",0);
        final String userID = preferences.getString("userID", "");
        final String mbr_token = preferences.getString("mbr_token", "");
        final String token = preferences.getString("token", "");

        if (!userID.isEmpty()&&!mbr_token.isEmpty()){
            registerForm.setVisibility(View.GONE);
            logoutForm.setVisibility(View.VISIBLE);
        }else {
            registerForm.setVisibility(View.VISIBLE);
            logoutForm.setVisibility(View.GONE);
        }
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preferences.edit().clear().commit();
                logout(userID,mbr_token,token);
            }
        });

        btnRegisterOtu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valueNama =  txtNama.getText().toString();
                valuePassword = txtPassword.getText().toString();
                valuePin = txtPin.getText().toString();
                valueUplineID = txtUplineID.getText().toString();
                valueUserID = txtUserID.getText().toString();
                valueEmail = txtEmail.getText().toString();
                if (!valueNama.isEmpty()||!valuePassword.isEmpty()||!valueUplineID.isEmpty()||!valuePin.isEmpty()
                        ||!valueUserID.isEmpty()||!valueEmail.isEmpty())
                {
                    register("OWAIDRIVER",valueEmail,valueNama,valuePassword,valuePin,valueUplineID,valueUserID);
                }else {
                    Snackbar.make(rootView.findViewById(R.id.main), "Please, Completed Form Data!", Snackbar.LENGTH_SHORT).show();
                }



            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), LoginOtuActivity.class));
            }
        });
    }

    private void logout(String userID,String mbr_token, String token) {
        retrofit2.Call<ResponseRegisterOtu> users = otuService.logout(userID,mbr_token,token);
        users.enqueue(new Callback<ResponseRegisterOtu>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseRegisterOtu> call, retrofit2.Response<ResponseRegisterOtu> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals("SUCCESS"))
                        Snackbar.make(rootView.findViewById(R.id.main), "Logout Successful", Snackbar.LENGTH_SHORT).show();
                    else {
                        Snackbar.make(rootView.findViewById(R.id.main), "Session Finish", Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseRegisterOtu> call, Throwable t) {
                Log.e("TAG", "onFailure: " + t.toString());
                t.printStackTrace();
                Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void register(String valueAplUse,String valueEmail, String valueNama, String valuePassword,
                          String valuePin,String valueUplineID,String valueUserID) {
        retrofit2.Call<ResponseRegisterOtu> users = otuService.registerOtu(valueAplUse,valueEmail,valueNama,valuePassword,valuePin,valueUplineID,valueUserID);
        users.enqueue(new Callback<ResponseRegisterOtu>() {
            @Override
            public void onResponse(retrofit2.Call<ResponseRegisterOtu> call, retrofit2.Response<ResponseRegisterOtu> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals("SUCCESS"))
                        Snackbar.make(rootView.findViewById(R.id.main), "Register Successful", Snackbar.LENGTH_SHORT).show();
                    else {
                        Snackbar.make(rootView.findViewById(R.id.main), ""+ response.body().getRespMessage() , Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ResponseRegisterOtu> call, Throwable t) {
                Log.e("TAG", "onFailure: " + t.toString());
                t.printStackTrace();
                Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        });


    }

}
