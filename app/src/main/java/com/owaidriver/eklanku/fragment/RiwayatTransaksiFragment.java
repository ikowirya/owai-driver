package com.owaidriver.eklanku.fragment;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.owaidriver.eklanku.MainActivity;
import com.owaidriver.eklanku.R;
import com.owaidriver.eklanku.adapter.ItemListener;
import com.owaidriver.eklanku.adapter.RiwayatTransaksiAdapter;
import com.owaidriver.eklanku.database.DBHandler;
import com.owaidriver.eklanku.database.Queries;
import com.owaidriver.eklanku.model.RiwayatTransaksi;
import com.owaidriver.eklanku.network.HTTPHelper;
import com.owaidriver.eklanku.network.Log;
import com.owaidriver.eklanku.network.NetworkActionResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

//import net.gumcode.drivermangjek.preference.UserPreference;


public class RiwayatTransaksiFragment extends Fragment{
    private static final String TAG = RiwayatTransaksiFragment.class.getSimpleName();
    private View rootView;
    MainActivity activity;
    ArrayList<RiwayatTransaksi> arrRiwayat;
    private ItemListener.OnItemTouchListener onItemTouchListener;
    private RecyclerView reviRiwayat;
    private RiwayatTransaksiAdapter riwayatAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private SwipeRefreshLayout swipe;
    Queries que;
    String page;
    int maxRetry = 4;
    int i=1;

    public RiwayatTransaksiFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.riwayat_transaksi_fragments, container, false);

        activity = (MainActivity) getActivity();
        activity.getSupportActionBar().setTitle("Transaction History");
        final Button btnShowMore = rootView.findViewById(R.id.btnShowMore);
        que = new Queries(new DBHandler(activity));
        reviRiwayat = rootView.findViewById(R.id.reviRiwayat);
        reviRiwayat.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(activity);
        arrRiwayat = que.getAllRiwayatTransaksi();

        SharedPreferences preferences = getActivity().getSharedPreferences("MyPref",0);
        page = preferences.getString("totalPage","1");
        final int totalPage = Integer.parseInt(page);
        btnShowMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( i < totalPage){
                    i+=1;
                    initData();
                    android.util.Log.d(TAG, "pageNow"+i);
                }
                else {
                    btnShowMore.setVisibility(View.GONE);
//                    android.util.Log.d(TAG, "halo"+i);
                }
            }
        });

//        initData();
        initListener();
        updateListView();

        swipe = rootView.findViewById(R.id.swipeFeedback);
        swipe.setRefreshing(false);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                i = 1;
                initData();
            }
        });
        swipe.post(new Runnable() {
            @Override
            public void run() {

                initData();
            }
        });

        return rootView;
    }

    private void initData(){
        android.util.Log.d(TAG, "pageNow"+i);
        final ProgressDialog sl = showLoading();
        JSONObject jFeed = new JSONObject();
        try {
            jFeed.put("id", que.getDriver().id);
            jFeed.put("page", i);
            jFeed.put("limit", 20);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HTTPHelper.getInstance(activity).getRiwayatTransaksi(jFeed, new NetworkActionResult() {
            @Override
            public void onSuccess(JSONObject obj) {
                arrRiwayat = HTTPHelper.getInstance(activity).parseRiwayatTransaksi(obj);
                que.truncate(DBHandler.TABLE_RIWAYAT_TRANSAKSI);
                que.insertRiwayatTransaksi(arrRiwayat);
                swipe.setRefreshing(false);
                updateListView();
                sl.dismiss();
                maxRetry = 4;
            }

            @Override
            public void onFailure(String message) {
            }

            @Override
            public void onError(String message) {
                if(maxRetry == 0){
                    sl.dismiss();
                    Toast.makeText(activity, "Connection problem..", Toast.LENGTH_SHORT).show();
                    maxRetry = 4;
                    swipe.setRefreshing(false);
                }else{
                    initData();
                    maxRetry--;
                    Log.d("Try_ke_feedback", String.valueOf(maxRetry));
                    sl.dismiss();
                }
            }
        });
    }

    private void initListener() {
        onItemTouchListener = new ItemListener.OnItemTouchListener() {
            @Override
            public void onCardViewTap(View view, int position) {
            }

            @Override
            public void onButton1Click(View view, int position) {
            }

            @Override
            public void onButton2Click(View view, int position) {
            }
        };
    }

    private void updateListView(){
        reviRiwayat.setLayoutManager(mLayoutManager);
        arrRiwayat = que.getAllRiwayatTransaksi();
        riwayatAdapter = new RiwayatTransaksiAdapter(arrRiwayat, onItemTouchListener, activity);
        reviRiwayat.setAdapter(riwayatAdapter);
        reviRiwayat.setVerticalScrollbarPosition(arrRiwayat.size()-1);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        que.closeDatabase();
    }

    private ProgressDialog showLoading(){
        ProgressDialog ad = ProgressDialog.show(activity, "", "Loading...", true);
        return ad;
    }

}
